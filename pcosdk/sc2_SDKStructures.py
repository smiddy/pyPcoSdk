import ctypes
from ctypes.wintypes import WORD, DWORD

# Define the pco Structures as in sc2_SDKStructures.h
PCO_MAXVERSIONHW = 10
PCO_MAXVERSIONFW = 10

class PCO_SC2_Hardware_DESC(ctypes.Structure):
    """ Class for C struct PCO_SC2_Hardware_DESC (sc2_SDKStructures.h)
    """
    _fields_ = [('szName', ctypes.c_char * 16),
                ('wBatchNo', WORD),
                ('wRevision', WORD),
                ('wVariant', WORD),
                ('ZZwDummy', WORD * 20)
                ]

    def __init__(self):
        super(PCO_SC2_Hardware_DESC, self).__init__()

class PCO_SC2_Firmware_DESC(ctypes.Structure):
    """ Class for C struct PCO_SC2_Firmware_DESC (sc2_SDKStructures.h)
    """
    _fields_ = [('szName', ctypes.c_char * 16),
                ('bMinorRev', ctypes.c_byte),
                ('bMajorRev', ctypes.c_byte),
                ('wVariant', WORD),
                ('ZZwDummy', WORD * 22)
                ]

    def __init__(self):
        super(PCO_SC2_Firmware_DESC, self).__init__()

class PCO_HW_Vers(ctypes.Structure):
    """ Class for C struct PCO_HW_Vers (sc2_SDKStructures.h)
    """
    _fields_ = [('BoardNum', WORD),
                ('Board', PCO_SC2_Hardware_DESC * PCO_MAXVERSIONHW)
                ]

    def __init__(self):
        super(PCO_HW_Vers, self).__init__()   

class PCO_FW_Vers(ctypes.Structure):
    """ Class for C struct PCO_FW_Vers (sc2_SDKStructures.h)
    """
    _fields_ = [('DeviceNum', WORD),
                ('Device', PCO_SC2_Firmware_DESC * PCO_MAXVERSIONFW)
                ]
    def __init__(self):
        super(PCO_FW_Vers, self).__init__()    

class PCO_CameraType(ctypes.Structure):
    """ Class for C struct PCO_CameraType (sc2_SDKStructures.h)
    """
    _fields_ = [('wSize', WORD),
                ('wCamType', WORD),
                ('wCamSubType', WORD),
                ('ZZwAlignDummy1', WORD),
                ('dwSerialNumber', DWORD),
                ('dwHWVersion', DWORD),
                ('dwFWVersion', DWORD),
                ('wInterfaceType', WORD),
                ('strHardwareVersion', PCO_HW_Vers),
                ('strFirmwareVersion', PCO_FW_Vers),
                ('ZZwDummy', WORD * 39)
                ]
    def __init__(self):
        super(PCO_CameraType, self).__init__()
        self.strHardwareVersion = PCO_HW_Vers()
        self.strFirmwareVersion = PCO_FW_Vers()
        self.wSize = ctypes.sizeof(self)

class PCO_General(ctypes.Structure):
    """ Class for C struct PCO_General (sc2_SDKStructures.h)
    """
    _fields_ = [('wSize', WORD),
                ('ZZwAlignDummy1', WORD),
                ('strCamType', PCO_CameraType),
                ('dwCamHealthWarnings', DWORD),
                ('dwCamHealthErrors', DWORD),
                ('dwCamHealthStatus', DWORD),
                ('sCCDTemperature', ctypes.c_short),
                ('sCamTemperature', ctypes.c_short),
                ('sPowerSupplyTemperature', ctypes.c_short),
                ('ZZwDummy', WORD * 37)
                ]

    def __init__(self):
        super(PCO_General, self).__init__()
        self.strCamType = PCO_CameraType()
        self.wSize = ctypes.sizeof(self)
