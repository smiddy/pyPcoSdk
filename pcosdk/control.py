import ctypes
from ctypes.wintypes import WORD, DWORD, SHORT, HANDLE
import logging
import warnings
import pcoError
import threading
import numpy as np
from .sc2_SDKStructures import PCO_General
import datetime
from pcosdk.version import __version__


# create logger
logger = logging.getLogger(__name__)
# Load pco.sdk library
pcodll = ctypes.cdll.LoadLibrary(
    r"C:\Program Files (x86)\Digital Camera Toolbox\pco.sdk\bin64\SC2_Cam.dll")
# Setup some type definitions
cInt = ctypes.c_int
int32 = ctypes.c_long
uInt32 = ctypes.c_ulong
uInt64 = ctypes.c_ulonglong
float64 = ctypes.c_double


class PcoSdkError(Exception):
    """
    Pcosdk expection handle
    """

    def __init__(self, err):
        self.errStr = pcoError.getText(err)
        self.logger = logging.getLogger(__name__)
        self.logger.error(self.errStr)

    def __str__(self):
        return self.errStr


class PcoCam(threading.Thread):
    """Python wrapper for pco.sdk from pco GmbH.

    Description

    :requires: pco.sdk (>=v6.01.04), pco.camware(>=v1.21)

    :param wCamNUm: number of camera. Default: 0
    :type wCamNum: int or ctypes.wintypes.WORD
    :param hdriver: Filehandle of the opened driver. Default: None
    :type hdriver: ctypes.wintypes.HANDLE


    Example::

        from dicamsdk import *

        board = 0 ...

    """

    def __init__(self, wCamNum: WORD = WORD(0), hcam: HANDLE = HANDLE(None)):
        """
        Constructor
        """
        threading.Thread.__init__(self)
        self.logger = logging.getLogger(__name__)
        if int is type(wCamNum):
            self.wCamNum = WORD(wCamNum)
        elif WORD is type(wCamNum):
            self.wCamNum = wCamNum
        else:
            raise TypeError('wCamNum must be int or ctypes.wintypes.WORD')
        if HANDLE != type(hcam):
            raise TypeError("hcam must be of type HANDLE")
        self.hcam = hcam
        # Initialize the camera
        self.PCO_OpenCamera(self.wCamNum, self.hcam)
        self.strGeneral = PCO_General()
        self.PCO_GetGeneral()
        self.wRecState = WORD(255)
        self.PCO_GetRecordingState()
        if 0 != self.wRecState.value:
            warnings.warn('Camera in Recording state, stopping now.')
            logger.warning('Camera in Recording state, stopping now.')
            self.PCO_SetRecordingState(0)
            self.PCO_GetRecordingState()
            if 0 != self.wRecState.value:
                self.logger.exception(
                    "pcosdk: Camera is in recording state.", exc_info=True)
                raise RuntimeError("pcosdk: Camera is in recording state.")
        self.PCO_ResetSettingsToDefault()
        self.dwWarn = DWORD(1)
        self.dwErr = DWORD(1)
        self.dwStatus = DWORD(1)
        self.PCO_GetCameraHealthStatus()
        self.wNoiseFilterMode = WORD(100)
        self.PCO_GetNoiseFilterMode()
        self.dwTime_s = DWORD(0)
        self.dwTime_ns = DWORD(0)
        #         self.PCO_GetCOCRunTime()
        self.dwDelay = DWORD(0)
        self.dwExposure = DWORD(0)
        self.wTimeBaseDelay = WORD(255)
        self.wTimeBaseExposure = WORD(255)
        self.PCO_GetDelayExposureTime()
        self.wTriggerMode = WORD(255)
        self.PCO_GetTriggerMode()
        self.wStorageMode = WORD(255)
        self.PCO_GetStorageMode()
        self.wRecSubmode = WORD(255)
        self.PCO_GetRecorderSubmode()
        self.wAcquMode = WORD(255)
        self.PCO_GetAcquireMode()
        self.PCO_SetDateTime()
        self.wTimestampMode = WORD(255)
        self.PCO_GetTimestampMode()
        self.dwRamSize = (DWORD * 4)()
        self.wPageSize = (WORD * 4)()
        self.PCO_GetCameraRamSize()
        self.wActSeg = WORD(255)
        self.PCO_GetActiveRamSegment()
        self.wBitAlignment = WORD(255)
        self.PCO_GetBitAlignment()
        self.wXResAct = WORD(0)
        self.wYResAct = WORD(0)
        self.wXResMax = WORD(0)
        self.wYResMax = WORD(0)
        self.PCO_GetSizes()
        self.wRoiX0 = WORD(0)
        self.wRoiY0 = WORD(0)
        self.wRoiX1 = WORD(0)
        self.wRoiY1 = WORD(0)
        self.PCO_GetROI()
        self.pixelRate = DWORD(0)
        self.PCO_GetPixelRate()
        self.wDoubleImage = WORD(255)
        self.PCO_GetDoubleImageMode()
        self.wBinHorz = WORD(0)
        self.wBinVert = WORD(0)
        self.PCO_GetBinning()
        self.bufList = []
        self.__version__ = __version__
        self.logger.info("Camera successfull initialized")

    def __enter__(self):
        return self

    # noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
    def __exit__(self, exc_type, exc_value, traceback):
        for bffr in self.bufList:
            self.errCheck(pcodll.PCO_FreeBuffer(self.hcam, bffr))
        self.PCO_CancelImages()
        self.PCO_CloseCamera()

    # noinspection PyMethodMayBeStatic
    def errCheck(self, err):
        """Returns the error number from pcoError

        Takes the provided error number and uses the function getText(err)
        from module pcoError to retrieve the error message.
        If the error code is nor an error neither a warning, the function
        raises a RuntimeError.

        :param err: Number with the error of the another method
        :type: ctypes.wintypes.DWORD
        :return: errcode : DWORD

        """
        if int != type(err):
            raise TypeError('err must be int')
        if 0 != err:
            err = DWORD(err)
            errHex = hex(err.value)
            # errors start with 0x08
            # warning start with 0xC0
            if errHex.startswith('0xC0'):
                warnStr = pcoError.getText(err.value)
                self.logger.warn(warnStr)
                warnings.warn(warnStr)
                return err
            else:
                raise PcoSdkError(err.value)
        return err

    def PCO_OpenCamera(self, wCamNum: DWORD = None, hcam: HANDLE = None):
        """Opens the camera

        :param wCamNum: Number of camera. Default is self.wCamNum
        :type: DWORD
        :param hcam: Camera handle. Default is self.hcam
        :type: HANDLE
        """
        if wCamNum is None:
            wCamNum = self.wCamNum
        if hcam is None:
            hcam = self.hcam
        pcodll.PCO_OpenCamera.argtypes = (ctypes.POINTER(HANDLE), WORD)
        pcodll.PCO_OpenCamera.restype = DWORD
        self.errCheck(pcodll.PCO_OpenCamera(ctypes.byref(hcam), wCamNum))
        self.logger.info('camera {} has been opened.'.format(wCamNum.value))

    def PCO_CloseCamera(self, hcam: HANDLE = None):
        """Closes the camera

        :param hcam: Camera handle. Default is self.hcam
        :type: HANDLE
        """
        if hcam is None:
            hcam = self.hcam
        pcodll.PCO_CloseCamera.argtypes = [HANDLE]
        pcodll.PCO_CloseCamera.restype = DWORD
        self.errCheck(pcodll.PCO_CloseCamera(hcam))
        self.logger.info(
            'camera {} has been closed.'.format(self.wCamNum.value))

    def PCO_GetGeneral(self):
        """Get general information about the camera and writes it to self.strGeneral
        """
        pcodll.PCO_GetGeneral.argtypes = (HANDLE, ctypes.POINTER(PCO_General))
        pcodll.PCO_GetGeneral.restype = DWORD
        self.errCheck(pcodll.PCO_GetGeneral(
            self.hcam, ctypes.byref(self.strGeneral)))
        self.logger.debug('General information have been read.')

    def PCO_GetRecordingState(self):
        """ Get the current recording state and saves it
        """
        pcodll.PCO_GetRecordingState.argtypes = (HANDLE, ctypes.POINTER(WORD))
        pcodll.PCO_GetRecordingState.restype = DWORD
        self.errCheck(pcodll.PCO_GetRecordingState(
            self.hcam, ctypes.byref(self.wRecState)))
        self.logger.debug('Recording state acquired.')

    def PCO_SetRecordingState(self, wRecState: (WORD, int)):
        """ Set the current recording state
        """
        if type(wRecState) == int:
            wRecState = WORD(wRecState)
        pcodll.PCO_SetRecordingState.argtypes = (HANDLE, WORD)
        pcodll.PCO_SetRecordingState.restype = DWORD
        self.errCheck(pcodll.PCO_SetRecordingState(self.hcam, wRecState))
        # Check for the state
        self.PCO_GetRecordingState()
        if wRecState.value != self.wRecState.value:
            self.logger.exception("Could not set wRecState.")
        self.wRecState = wRecState
        self.logger.debug('Recording state set.')

    def PCO_ResetSettingsToDefault(self):
        """Reset the setting to default
        """
        pcodll.PCO_ResetSettingsToDefault.argtype = [HANDLE]
        pcodll.PCO_ResetSettingsToDefault.restype = DWORD
        self.errCheck(pcodll.PCO_ResetSettingsToDefault(self.hcam))
        self.logger.info("Camera have been reset")

    def PCO_GetTimestampMode(self):
        """ Gets the timestamp mode
        """
        pcodll.PCO_GetTimestampMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetTimestampMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetTimestampMode(
            self.hcam, ctypes.byref(self.wTimestampMode)))
        self.logger.debug("Timestamp mode received.")

    def PCO_SetTimestampMode(self, wTimestampMode: (int, WORD)):
        """Sets the timestamp mode.
        0: OFF
        1: binary
        2: binary + ascii
        3: ASCII text only

        :param wTimestampMode: Desired timestamp
        :type: WORD or int
        """
        if type(wTimestampMode) == int:
            wTimestampMode = WORD(wTimestampMode)
        pcodll.PCO_SetTimestampMode.argtypes = (HANDLE, WORD)
        pcodll.PCO_SetTimestampMode.restype = DWORD
        self.errCheck(pcodll.PCO_SetTimestampMode(self.hcam, wTimestampMode))
        self.PCO_GetTimestampMode()
        self.logger.info(
            "Timestampmode set to {}".format(wTimestampMode.value))

    def PCO_GetCameraRamSize(self):
        """Returns the size of the camera internal memory in CamRAM pages and the CamRAM page size in pixels.
        """
        pcodll.PCO_GetCameraRamSize.argtypes = (HANDLE, ctypes.POINTER(DWORD), ctypes.POINTER(WORD))
        pcodll.PCO_GetCameraRamSize.restype = DWORD
        self.errCheck(
            pcodll.PCO_GetCameraRamSize(self.hcam,
                                        ctypes.cast(self.dwRamSize, ctypes.POINTER(DWORD)),
                                        ctypes.cast(self.wPageSize, ctypes.POINTER(WORD))))
        self.logger.debug("CameraRamSize acquired.")

    def PCO_ClearRamSegment(self):
        """Clears the *active* memory segment. All image data is cleared and

        :return: <None>
        """
        pcodll.PCO_ClearRamSegment.argtype = HANDLE
        pcodll.PCO_ClearRamSegment.restype = DWORD
        self.errCheck(pcodll.PCO_ClearRamSegment(self.hcam))
        self.logger.info("Active camRam segment number {} cleared.".format(self.wActSeg.value))

    def PCO_GetActiveRamSegment(self):
        """Returns the active memory segment of the camera

        :return: <DWORD> self.wActSeg
        """
        pcodll.PCO_GetActiveRamSegment.argtypes = (HANDLE, ctypes.POINTER(WORD))
        pcodll.PCO_GetActiveRamSegment.restype = DWORD
        self.errCheck(pcodll.PCO_GetActiveRamSegment(self.hcam, ctypes.byref(self.wActSeg)))
        self.logger.debug("Active camRam segment is {}".format(self.wActSeg.value))

    def PCO_SetActiveRamSegment(self, wActSeg):
        """Sets the active memory segment of the camera

        :param wActSeg: <WORD> Ram Segment to be activated
        :return: <DWORD> self.wActSeg
        """
        if type(wActSeg) == int:
            wActSeg = WORD(wActSeg)
        pcodll.PCO_SetActiveRamSegment.argtypes = (HANDLE, WORD)
        pcodll.PCO_SetActiveRamSegment.restype = DWORD
        self.errCheck(pcodll.PCO_SetActiveRamSegment(self.hcam, wActSeg))
        self.wActSeg = wActSeg
        self.logger.info("Active camRam segment set to {}".format(self.wActSeg.value))

    def PCO_GetNumberOfImagesInSegment(self, wSegment: (int, WORD)):
        """Returns the number of valid images and the maximum number of images within a distinct segment

        :param wSegment: Desired Segment
        :type: int, WORD
        :rtype: int, int
        :return: dwValidImageCnt, dwMaxImageCnt
        """
        if type(wSegment) == int:
            wSegment = WORD(wSegment)
        dwValidImageCnt = DWORD(0)
        dwMaxImageCnt = DWORD(0)
        pcodll.PCO_GetNumberOfImagesInSegment.argtypes = [
            HANDLE, WORD, ctypes.POINTER(DWORD), ctypes.POINTER(DWORD)]
        pcodll.PCO_GetNumberOfImagesInSegment.restype = DWORD
        self.errCheck(pcodll.PCO_GetNumberOfImagesInSegment(
            self.hcam, wSegment, ctypes.byref(dwValidImageCnt), ctypes.byref(dwMaxImageCnt)))
        self.logger.debug("Number of valid images in segment acquired.")
        return dwValidImageCnt.value, dwMaxImageCnt.value

    def PCO_ArmCamera(self):
        """ Arms the camera
        """
        pcodll.PCO_ArmCamera.argtypes = [HANDLE]
        pcodll.PCO_ArmCamera.restype = DWORD
        self.errCheck(pcodll.PCO_ArmCamera(self.hcam))
        self.logger.info("Camera armed.")

    def PCO_GetCameraHealthStatus(self):
        """ Interrogates the health status of the camera
        """
        pcodll.PCO_GetCameraHealthStatus.argtypes = (HANDLE, ctypes.POINTER(
            DWORD), ctypes.POINTER(DWORD), ctypes.POINTER(DWORD))
        pcodll.PCO_GetCameraHealthStatus.restype = DWORD
        self.errCheck(pcodll.PCO_GetCameraHealthStatus(self.hcam, ctypes.byref(
            self.dwWarn), ctypes.byref(self.dwErr), ctypes.byref(self.dwStatus)))
        self.logger.debug("Camera Health Status checked.")

    def PCO_GetTemperature(self):
        """Gets the temperature from camera in degree Celsius

        :returns: sCCDTemp, sCamTemp, sPowTemp
        :rtype: float, int, int
        """
        sCCDTemp = ctypes.c_short(-10000)
        sCamTemp = ctypes.c_short(-10000)
        sPowTemp = ctypes.c_short(-10000)
        pcodll.PCO_GetTemperature.argtypes = [HANDLE, ctypes.POINTER(
            ctypes.c_short), ctypes.POINTER(ctypes.c_short), ctypes.POINTER(ctypes.c_short)]
        pcodll.PCO_GetTemperature.restype = DWORD
        self.errCheck(pcodll.PCO_GetTemperature(self.hcam, ctypes.byref(
            sCCDTemp), ctypes.byref(sCamTemp), ctypes.byref(sPowTemp)))
        self.logger.debug("Temperature readout successful.")
        # sCCDTemp is provided in tenth of a degree, since the computation
        return sCCDTemp.value / 10, sCamTemp.value, sPowTemp.value

    def PCO_GetNoiseFilterMode(self):
        """Returns the current operating mode for image correction in the camera

        0 (0x0000): off
        1 (0x0001): on
        257 (0x0101): on + hot pixel correction
        """
        pcodll.PCO_GetNoiseFilterMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetNoiseFilterMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetNoiseFilterMode(
            self.hcam, ctypes.byref(self.wNoiseFilterMode)))
        self.logger.debug('NoiseFilterMode received.')

    def PCO_SetNoiseFilterMode(self, wNoiseFilterMode: (int, WORD)):
        """Sets the current operating mode for image correction in the camera

        0 (0x0000): off
        1 (0x0001): on
        257 (0x0101): on + hot pixel correction

        :param wNoiseFilterMode: Desired noise filter mode
        :type: WORD or int
        """
        if type(wNoiseFilterMode) == int:
            wNoiseFilterMode = WORD(wNoiseFilterMode)
        pcodll.PCO_SetNoiseFilterMode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetNoiseFilterMode.restype = DWORD
        self.errCheck(pcodll.PCO_SetNoiseFilterMode(
            self.hcam, wNoiseFilterMode))
        self.PCO_GetNoiseFilterMode()
        self.logger.info('NoiseFilterMode set to {}.'.format(
            wNoiseFilterMode.value))

    def PCO_GetCOCRunTime(self):
        """Returns values for the time needed to acquire a single image
        """
        pcodll.PCO_GetCOCRunTime.argtypes = [
            HANDLE, ctypes.POINTER(DWORD), ctypes.POINTER(DWORD)]
        pcodll.PCO_GetCOCRunTime.restype = DWORD
        self.errCheck(pcodll.PCO_GetCOCRunTime(self.hcam, ctypes.byref(
            self.dwTime_s), ctypes.byref(self.dwTime_ns)))
        self.logger.debug('COCRunTime received.')

    def PCO_GetDelayExposureTime(self):
        """Returns the current setting of the delay and exposure time
        """
        pcodll.PCO_GetDelayExposureTime.argtypes = [HANDLE, ctypes.POINTER(
            DWORD), ctypes.POINTER(DWORD), ctypes.POINTER(WORD), ctypes.POINTER(WORD)]
        pcodll.PCO_GetDelayExposureTime.restype = DWORD
        self.errCheck(pcodll.PCO_GetDelayExposureTime(self.hcam, ctypes.byref(self.dwDelay), ctypes.byref(
            self.dwExposure), ctypes.byref(self.wTimeBaseDelay), ctypes.byref(self.wTimeBaseExposure)))
        self.logger.debug("Delay and exposure time received.")

    def PCO_SetDelayExposureTime(self, dwDelay: (int, DWORD), dwExposure: (int, DWORD), wTimeBaseDelay: (int, WORD),
                                 wTimeBaseExposure: (int, WORD)):
        """Sets the delay and exposure time
        """
        if type(dwExposure) == int:
            dwExposure = DWORD(dwExposure)
        if type(dwDelay) == int:
            dwDelay = DWORD(dwDelay)
        if type(wTimeBaseDelay) == int:
            wTimeBaseDelay = WORD(wTimeBaseDelay)
        if type(wTimeBaseExposure) == int:
            wTimeBaseExposure = WORD(wTimeBaseExposure)
        pcodll.PCO_SetDelayExposureTime.argtypes = [
            HANDLE, DWORD, DWORD, WORD, WORD]
        pcodll.PCO_SetDelayExposureTime.restype = DWORD
        self.errCheck(pcodll.PCO_SetDelayExposureTime(
            self.hcam, dwDelay, dwExposure, wTimeBaseDelay, wTimeBaseExposure))
        self.PCO_GetDelayExposureTime()
        self.logger.info("Delay and exposure time set to ({}, {}, {}, {}).".format(
            dwDelay.value, dwExposure.value, wTimeBaseDelay.value, wTimeBaseExposure.value))

    def PCO_GetTriggerMode(self):
        """Returns the current trigger mode
        """
        pcodll.PCO_GetTriggerMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetTriggerMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetTriggerMode(
            self.hcam, ctypes.byref(self.wTriggerMode)))
        self.logger.debug("Trigger mode received.")

    def PCO_SetTriggerMode(self, wTriggerMode: (int, WORD)):
        """Sets the current trigger mode.

        Valid values are
        0 (0x0000) = [auto sequence]
        1 (0x0001) = [software trigger]
        2 (0x0002) = [external exposure start & software trigger]
        3 (0x0003) = [external exposure control]
        4 (0x0004) = [external synchronized]
        5 (0x0005) = [fast external exposure control]
        6 (0x0006) = [external CDS control]
        7 (0x0007) = [slow external exposure control]
        258 (0x0102) = [external synchronized HDSDI]

        :param wTriggerMode: trigger mode
        :type: int or WORD
        """
        if type(wTriggerMode) == int:
            wTriggerMode = WORD(wTriggerMode)
        pcodll.PCO_SetTriggerMode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetTriggerMode.restype = DWORD
        self.errCheck(pcodll.PCO_SetTriggerMode(self.hcam, wTriggerMode))
        self.PCO_GetTriggerMode()
        self.logger.info("Trigger mode set to {}".format(wTriggerMode.value))

    def PCO_ForceTrigger(self):
        """Does start an exposure if the trigger mode is either [software trigger]
        or [extern exposure & software trigger]. In all other modes the command has no effect.

        Returns 0 if trigger was unsuccessful due to busyness of camera or 1 if successful

        :rtype: int
        :return: wTriggered
        """
        wTriggered = WORD(255)
        pcodll.PCO_ForceTrigger.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_ForceTrigger.restype = DWORD
        self.errCheck(pcodll.PCO_ForceTrigger(
            self.hcam, ctypes.byref(wTriggered)))
        if not wTriggered.value:
            self.logger.warn(
                "ForceTrigger did not trigger new image exposure.")
            warnings.warn("ForceTrigger did not trigger new image exposure.")
        else:
            self.logger.debug('ForceTrigger successful.')
        return wTriggered.value

    def PCO_GetCameraBusyStatus(self):
        """Returns the current busy status of the camera.

        0 if camera is not busy
        1 if camera is busy

        :return: wCameraBusyState: camera busy status
        :rtype: int
        """
        wCameraBusyState = WORD(255)
        pcodll.PCO_GetCameraBusyStatus.argtypes = [
            HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetCameraBusyStatus.restype = DWORD
        self.errCheck(pcodll.PCO_GetCameraBusyStatus(
            self.hcam, ctypes.byref(wCameraBusyState)))
        self.logger.debug("CameraBusyStatus received.")
        return wCameraBusyState.value

    def PCO_GetStorageMode(self):
        """Returns the current storage mode of the camera

        0: recorder mode
        1: FIFO buffer mode
        """
        pcodll.PCO_GetStorageMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetStorageMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetStorageMode(
            self.hcam, ctypes.byref(self.wStorageMode)))
        self.logger.debug("Storage mode received.")

    def PCO_SetStorageMode(self, wStorageMode: (int, WORD)):
        """Sets the storage mode of the camera

        0: recorder mode
        1: FIFO buffer mode

        :param wStorageMode: camera storage mode
        :type: int or WORD
        """
        if type(wStorageMode) == int:
            wStorageMode = WORD(wStorageMode)
        pcodll.PCO_SetStorageMode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetStorageMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetStorageMode(self.hcam, wStorageMode))
        self.PCO_GetStorageMode()
        self.logger.debug("Storage mode set to {}.".format(wStorageMode.value))

    def PCO_GetRecorderSubmode(self):
        """Returns the current recorder submode

        0: sequence
        1: ring mode
        """
        pcodll.PCO_GetRecorderSubmode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetRecorderSubmode.restype = DWORD
        self.errCheck(pcodll.PCO_GetRecorderSubmode(
            self.hcam, ctypes.byref(self.wRecSubmode)))
        self.logger.debug("Recorder submode received.")

    def PCO_SetRecorderSubmode(self, wRecSubmode: (int, WORD)):
        """Sets the current recorder submode

        0: sequence
        1: ring mode

        :param wRecSubmode: recorder submode
        :type: int or WORD
        """
        if type(wRecSubmode) == int:
            wRecSubmode = WORD(wRecSubmode)
        pcodll.PCO_SetRecorderSubmode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetRecorderSubmode.restype = DWORD
        self.errCheck(pcodll.PCO_SetRecorderSubmode(self.hcam, wRecSubmode))
        self.PCO_GetRecorderSubmode()
        self.logger.info(
            "Recorder submode set to {}.".format(wRecSubmode.value))

    def PCO_GetAcquireMode(self):
        """Returns the current acquire mode

        o: auto
        1: external
        2: external modulate
        """
        pcodll.PCO_GetAcquireMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetAcquireMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetAcquireMode(
            self.hcam, ctypes.byref(self.wAcquMode)))
        self.logger.debug("Acquire mode received.")

    def PCO_SetAcquireMode(self, wAcquMode: (int, WORD)):
        """Returns the current acquire mode

        o: auto
        1: external
        2: external modulate

        :param wAcquMode: recorder submode
        :type: int or WORD
        """
        if type(wAcquMode) == int:
            wAcquMode = WORD(wAcquMode)
        pcodll.PCO_SetAcquireMode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetAcquireMode.restype = DWORD
        self.errCheck(pcodll.PCO_SetAcquireMode(self.hcam, wAcquMode))
        self.PCO_GetAcquireMode()
        self.logger.info(
            "Acquire mode set to {}.".format(wAcquMode.value))

    def PCO_SetDateTime(self):
        """Sets date and time information for the internal camera clock.
        Current time is acquired with time.localtime()
        """
        # Create valid  time input
        curTime = datetime.datetime.now()
        ucDay = ctypes.wintypes.BYTE(curTime.day)
        ucMonth = ctypes.wintypes.BYTE(curTime.month)
        wYear = ctypes.wintypes.WORD(curTime.year)
        wHour = ctypes.wintypes.WORD(curTime.hour)
        ucMin = ctypes.wintypes.BYTE(curTime.minute)
        ucSec = ctypes.wintypes.BYTE(curTime.second)
        pcodll.PCO_SetDateTime.argtypes = [
            HANDLE, ctypes.wintypes.BYTE, ctypes.wintypes.BYTE, WORD, WORD, ctypes.wintypes.BYTE, ctypes.wintypes.BYTE]
        pcodll.PCO_SetDateTime.restype = DWORD
        self.errCheck(pcodll.PCO_SetDateTime(
            self.hcam, ucDay, ucMonth, wYear, wHour, ucMin, ucSec))
        self.logger.debug("Date and time set.")

    def PCO_GetSizes(self):
        """Returns the current armed images sizes of the camera
        """
        pcodll.PCO_GetSizes.argtypes = (HANDLE, ctypes.POINTER(
            WORD), ctypes.POINTER(WORD), ctypes.POINTER(WORD), ctypes.POINTER(WORD))
        pcodll.PCO_GetSizes.restype = DWORD
        self.errCheck(pcodll.PCO_GetSizes(self.hcam, ctypes.byref(self.wXResAct), ctypes.byref(self.wYResAct),
                                          ctypes.byref(self.wXResMax), ctypes.byref(self.wYResMax)))
        self.logger.debug("Image sizes received.")

    def PCO_GetBitAlignment(self):
        """ Returns the current bit alignment

        0: MSB
        1: LSB
        """
        pcodll.PCO_GetBitAlignment.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetBitAlignment.restype = DWORD
        self.errCheck(pcodll.PCO_GetBitAlignment(
            self.hcam, ctypes.byref(self.wBitAlignment)))
        self.logger.debug('Bit Alignment received.')

    def PCO_SetBitAlignment(self, wBitAlignment):
        """Sets the bit alignment

        :param wBitAlignment: 0 (MSB) or 1 (LSB)
        :type: int or WORD
        """
        if type(wBitAlignment) == int:
            wBitAlignment = WORD(wBitAlignment)
        pcodll.PCO_SetBitAlignment.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetBitAlignment.restype = DWORD
        self.errCheck(pcodll.PCO_SetBitAlignment(self.hcam, wBitAlignment))
        self.PCO_GetBitAlignment()
        self.logger.info("Bit Alignment set to {}".format(wBitAlignment.value))

    def PCO_AllocateBuffer(self, buffer):
        """Allocates the buffer
        """
        buffer.wBinHorz = self.wBinHorz
        buffer.wBinVert = self.wBinVert
        buffer.xRes = WORD(self.wXResAct.value)
        buffer.yRes = WORD(self.wYResAct.value)
        buffer.wTimestampMode = self.wTimestampMode
        buffer.wBitAlignment = self.wBitAlignment
        if self.wDoubleImage.value == 1:
            buffer.doubleImage = True
        buffer.size = buffer.xRes.value * \
                      buffer.yRes.value * ctypes.sizeof(WORD)
        buffer.buffer = (WORD * (buffer.xRes.value *
                                 buffer.yRes.value))()
        buffer.address = ctypes.cast(buffer.buffer, ctypes.POINTER(WORD))
        pcodll.PCO_AllocateBuffer.argtypes = [HANDLE, ctypes.POINTER(ctypes.c_short),
                                              DWORD, ctypes.POINTER(ctypes.POINTER(WORD)), ctypes.POINTER(HANDLE)]
        pcodll.PCO_AllocateBuffer.restype = DWORD
        pcodll.PCO_AllocateBuffer(self.hcam, ctypes.byref(
            buffer.number), buffer.size, ctypes.byref(buffer.address), ctypes.byref(buffer.hEvent))
        # Add to the allocated buffer list in case of error exit
        self.bufList.append(buffer.number)
        self.logger.debug(
            "Buffer with number {} allocated".format(buffer.number.value))

    def PCO_FreeBuffer(self, curBuf):
        """ Frees the buffer

        :param BufNum: number of buffer
        :type: ctypes.c_short
        """
        pcodll.PCO_FreeBuffer.argtypes = (HANDLE, ctypes.c_short)
        pcodll.PCO_FreeBuffer.restye = DWORD
        self.errCheck(pcodll.PCO_FreeBuffer(self.hcam, curBuf.number))
        self.bufList.pop(self.bufList.index(curBuf.number))
        self.logger.debug(
            "Buffer with number {} freed".format(curBuf.number.value))

    def PCO_GetBufferStatus(self, curBuf):
        """Queries the status of the buffer context of curBuf

        :return: dwStatusDLL, dwStatusDrv: status inside SDK and regarding image transfer
        :rtype: int
        """
        dwStatusDLL = DWORD()
        dwStatusDrv = DWORD()
        pcodll.PCO_GetBufferStatus.argtypes = [
            HANDLE, SHORT, ctypes.POINTER(DWORD), ctypes.POINTER(DWORD)]
        pcodll.PCO_GetBufferStatus.restype = DWORD
        self.errCheck(pcodll.PCO_GetBufferStatus(
            self.hcam, curBuf.number, ctypes.byref(dwStatusDLL), ctypes.byref(dwStatusDrv)))
        self.logger.debug(
            "Status of buffer {} queried".format(curBuf.number.value))
        return dwStatusDLL.value, dwStatusDrv.value

    def PCO_SetImageParameters(self, curBuf, dwFlags: WORD = DWORD(2)):
        """Sets the image parameters for internal allocated resources
        """
        pcodll.PCO_SetImageParameters.argtypes = (
            HANDLE, WORD, WORD, DWORD, ctypes.c_void_p, ctypes.c_int)
        pcodll.PCO_SetImageParameters.restype = DWORD
        self.errCheck(pcodll.PCO_SetImageParameters(self.hcam, curBuf.xRes,
                                                    curBuf.yRes, dwFlags, ctypes.c_void_p(None), ctypes.c_int(0)))
        self.logger.debug("Image parameers set.")

    def PCO_GetImageEx(self, curBuf, wSegment: WORD = WORD(1), dw1stImage: DWORD = DWORD(0),
                       wBitPerPixel: WORD = WORD(16)):
        """Get a single image from the camera
        """
        pcodll.PCO_GetImageEx.argtypes = (
            HANDLE, WORD, DWORD, DWORD, ctypes.c_short, WORD, WORD, WORD)
        pcodll.PCO_GetImageEx.restype = DWORD
        self.errCheck(pcodll.PCO_GetImageEx(self.hcam, wSegment, dw1stImage,
                                            dw1stImage, curBuf.number, curBuf.xRes, curBuf.yRes, wBitPerPixel))
        self.logger.info(
            'Image received from buffer {}'.format(curBuf.number.value))

    def PCO_AddBufferEx(self, curBuf, dw1stImage, dwLastImage):
        """ Set up a request for a single image transfer

        :param curBuf: buffer
        :type: pcosdk.ImgBuf
        :param dw1stImage: variable to select image number
        :type: int or DWORD
        :param dwLastImage: must be set to value as dw1stImage
        :type: int or DWORD
        """
        if type(dw1stImage) == int:
            dw1stImage = DWORD(dw1stImage)
        if type(dwLastImage) == int:
            dwLastImage = DWORD(dwLastImage)
        pcodll.PCO_AddBufferEx.argtypes = [
            HANDLE, DWORD, DWORD, SHORT, WORD, WORD, WORD]
        pcodll.PCO_AddBufferEx.restype = DWORD
        # TODO implement BitPerPixel
        self.logger.warning("BitPerPixel is hard coded to 16 in AddBufferEx")
        self.errCheck(pcodll.PCO_AddBufferEx(self.hcam, dw1stImage,
                                             dwLastImage, curBuf.number, curBuf.xRes, curBuf.yRes, 16))

    def PCO_AddBufferExtern(self, buffer, wActSeg=1, dw1stImage=0):
        """ Adds an external python buffer

        :param buffer:
        :param wActSeg:
        :param dw1stImage:
        :return:
        """
        if type(wActSeg) == int:
            wActSeg = WORD(wActSeg)
        if type(dw1stImage) == int:
            dw1stImage = DWORD(dw1stImage)
        buffer.wBinHorz = self.wBinHorz
        buffer.wBinVert = self.wBinVert
        buffer.xRes = WORD(self.wXResAct.value)
        buffer.yRes = WORD(self.wYResAct.value)
        buffer.wTimestampMode = self.wTimestampMode
        buffer.wBitAlignment = self.wBitAlignment
        if self.wDoubleImage.value == 1:
            buffer.doubleImage = True
        buffer.size = buffer.xRes.value * \
                      buffer.yRes.value * ctypes.sizeof(WORD)
        buffer.buffer = ctypes.create_string_buffer(buffer.size)
        buffer.address = ctypes.cast(buffer.buffer, ctypes.c_void_p)
        pcodll.PCO_AddBufferExtern.argtypes = (HANDLE, HANDLE, WORD, DWORD, DWORD, DWORD, ctypes.c_void_p,
                                               DWORD, ctypes.POINTER(DWORD))
        pcodll.PCO_AddBufferExtern.restype = DWORD
        dwStatus = DWORD(255)
        self.errCheck(pcodll.PCO_AddBufferExtern(self.hcam, buffer.hEvent, wActSeg, dw1stImage, dw1stImage, DWORD(0),
                                                 ctypes.byref(buffer.buffer), buffer.size, ctypes.byref(dwStatus)))
        return dwStatus

    def PCO_GetROI(self):
        """Returns the current ROI
        """
        pcodll.PCO_GetROI.argtypes = [HANDLE, ctypes.POINTER(WORD), ctypes.POINTER(
            WORD), ctypes.POINTER(WORD), ctypes.POINTER(WORD)]
        pcodll.PCO_GetROI.restype = DWORD
        self.errCheck(pcodll.PCO_GetROI(self.hcam, ctypes.byref(self.wRoiX0), ctypes.byref(
            self.wRoiY0), ctypes.byref(self.wRoiX1), ctypes.byref(self.wRoiY1)))
        self.logger.debug("Roi values received.")

    def PCO_SetROI(self, wRoiX0: (WORD, int), wRoiY0: (WORD, int), wRoiX1: (WORD, int), wRoiY1: (WORD, int)):
        """Sets the current ROI

        :param wRoiX0: first coordinate in horizontal direction
        :type: WORD or int
        :param wRoiY0: first coordinate in vertical direction
        :type: WORD or int
        :param wRoiX1: second coordinate in horizontal direction
        :type: WORD or int
        :param wRoiY1: second coordinate in vertical direction
        :type: WORD or int
        """
        # Change input to int for easier warning string format handling
        if type(wRoiX0) == WORD:
            wRoiX0 = wRoiX0.value
        if type(wRoiY0) == WORD:
            wRoiY0 = wRoiY0.value
        if type(wRoiX1) == WORD:
            wRoiX1 = wRoiX1.value
        if type(wRoiY1) == WORD:
            wRoiY1 = wRoiY1.value
        pcodll.PCO_SetROI.argtypes = [HANDLE, WORD, WORD, WORD, WORD]
        pcodll.PCO_SetROI.restype = DWORD
        self.errCheck(pcodll.PCO_SetROI(
            self.hcam, wRoiX0, wRoiY0, wRoiX1, wRoiY1))
        self.PCO_GetROI()
        # Check wether desired values can be applied to camera
        if not (wRoiX0, wRoiY0, wRoiX1, wRoiY1) == (
                self.wRoiX0.value, self.wRoiY0.value, self.wRoiX1.value, self.wRoiY1.value):
            warnings.warn("Could not set ROI to desired values. Input: ({},{},{},{}). Effective ({},{},{},{}).".format(
                wRoiX0, wRoiY0, wRoiX1, wRoiY1, self.wRoiX0.value, self.wRoiY0.value, self.wRoiX1.value,
                self.wRoiY1.value))
            self.logger.warn(
                "Could not set ROI to desired values. Input: ({},{},{},{}). Effective ({},{},{},{}).".format(
                    wRoiX0, wRoiY0, wRoiX1, wRoiY1, self.wRoiX0.value, self.wRoiY0.value, self.wRoiX1.value,
                    self.wRoiY1.value))
        self.logger.info('ROI set to {}, {}, {}, {}'.format(self.wRoiX0.value,
                                                            self.wRoiY0.value, self.wRoiX1.value, self.wRoiY1.value))

    def PCO_GetPixelRate(self):
        """Returns the current pixelrate in Hz
        """
        pcodll.PCO_GetPixelRate.argtypes = [HANDLE, ctypes.POINTER(DWORD)]
        pcodll.PCO_GetPixelRate.restype = DWORD
        self.errCheck(pcodll.PCO_GetPixelRate(
            self.hcam, ctypes.byref(self.pixelRate)))
        self.logger.debug(
            "Aquired pixelrate with {}Hz".format(self.pixelRate.value))

    def PCO_SetPixelRate(self, pixelRate: (DWORD, int)):
        """ Sets the pixelrate
        """
        pcodll.PCO_SetPixelRate.argtypes = (HANDLE, DWORD)
        pcodll.PCO_SetPixelRate.restype = DWORD
        self.errCheck(pcodll.PCO_SetPixelRate(self.hcam, pixelRate))
        self.PCO_GetPixelRate()
        self.logger.info("Pixelrate set to {}Hz".format(self.pixelRate.value))

    def PCO_GetDoubleImageMode(self):
        """Returns the setting for double image mode
        """
        pcodll.PCO_GetDoubleImageMode.argtypes = [HANDLE, ctypes.POINTER(WORD)]
        pcodll.PCO_GetDoubleImageMode.restype = DWORD
        self.errCheck(pcodll.PCO_GetDoubleImageMode(
            self.hcam, ctypes.byref(self.wDoubleImage)))
        self.logger.debug("doubleImageMode setting acquired.")

    def PCO_SetDoubleImageMode(self, wDoubleImage: (int, WORD)):
        """Sets double image mode

        :param wDoubleImage: set the mode (0=off, 1=on)
        :type: WORD or int
        """
        if type(wDoubleImage) == int:
            wDoubleImage = WORD(wDoubleImage)
        pcodll.PCO_SetDoubleImageMode.argtypes = [HANDLE, WORD]
        pcodll.PCO_SetDoubleImageMode.restype = DWORD
        self.errCheck(pcodll.PCO_SetDoubleImageMode(self.hcam, wDoubleImage))
        self.PCO_GetDoubleImageMode()
        if self.wDoubleImage.value != wDoubleImage.value:
            self.logger.error('wDoubleImage have not been set.')
            raise ValueError('wDoubleImage have not been set.')
        self.logger.debug(
            "doubleImageMode set to {}".format(wDoubleImage.value))

    def PCO_GetBinning(self):
        """Returns the values for binning
        """
        pcodll.PCO_GetBinning.argtypes = [
            HANDLE, ctypes.POINTER(WORD), ctypes.POINTER(WORD)]
        pcodll.PCO_GetBinning.restype = DWORD
        self.errCheck(pcodll.PCO_GetBinning(self.hcam, ctypes.byref(
            self.wBinHorz), ctypes.byref(self.wBinVert)))
        self.logger.debug("Binning values received.")

    def PCO_SetBinning(self, wBinHorz: (int, WORD), wBinVert: (int, WORD)):
        """Sets the values for binning
        """
        if type(wBinHorz) == int:
            wBinHorz = WORD(wBinHorz)
        if type(wBinVert) == int:
            wBinVert = WORD(wBinVert)
        pcodll.PCO_SetBinning.argtypes = [HANDLE, WORD, WORD]
        pcodll.PCO_SetBinning.restype = DWORD
        self.errCheck(pcodll.PCO_SetBinning(self.hcam, wBinHorz, wBinVert))
        self.PCO_GetBinning()
        self.logger.info("Binning values set to {} horizontal and {} vertical.".format(
            wBinHorz.value, wBinVert.value))

    def PCO_CancelImages(self):
        """Removes all remaining buffers from the internal queue, reset it and the
        tranfer state machine in the camera
        """
        pcodll.PCO_CancelImages.argtypes = [HANDLE]
        pcodll.PCO_CancelImages.restype = DWORD
        self.errCheck(pcodll.PCO_CancelImages(self.hcam))
        self.logger.debug("All images cancelled.")

    def buffer2Numpy(self, curBuf):
        """Reads out the image and timestamp from the buffer to numpy array.

        The function returns an ndarray for the image. If binary timestamp is activated
        a timestamp and the image count is extracted from the image, otherwise None.

        :param curBuf: buffer for readout
        :type: class imgBuf.ImgBuf
        :rtype: numpy.ndarray, datetime,datetime, int
        :return: npimg, timestamp, imgCount
        """
        dt = np.dtype(WORD)
        dt = dt.newbyteorder('<')
        npimg = np.frombuffer(curBuf.buffer, dtype=dt)
        # If MSB alignment is set, the buffer has to be shifted by to bit to
        # the right (p.162)
        if curBuf.wBitAlignment.value == 0:
            npimg = npimg >> 2
        if (curBuf.wTimestampMode.value == 1) or (curBuf.wTimestampMode.value == 2):
            # The information is stored in the lower byte, for 4 bits a digit of the final value
            # See sdk manual p.146
            imgCounter = (npimg[0] >> 4) * 10 ** 7 \
                         + (npimg[0] & 0b1111) * 10 ** 6 \
                         + (npimg[1] >> 4) * 10 ** 5 \
                         + (npimg[1] & 0b1111) * 10 ** 4 \
                         + (npimg[2] >> 4) * 10 ** 3 \
                         + (npimg[2] & 0b1111) * 10 ** 2 \
                         + (npimg[3] >> 4) * 10 ** 1 \
                         + (npimg[3] & 0b1111)
            year = (npimg[4] >> 4) * 10 ** 3 \
                   + (npimg[4] & 0b1111) * 10 ** 2 \
                   + (npimg[5] >> 4) * 10 \
                   + (npimg[5] & 0b1111)
            month = (npimg[6] >> 4) * 10 \
                    + (npimg[6] & 0b1111)
            day = (npimg[7] >> 4) * 10 \
                  + (npimg[7] & 0b1111)
            hour = (npimg[8] >> 4) * 10 \
                   + (npimg[8] & 0b1111)
            minute = (npimg[9] >> 4) * 10 \
                     + (npimg[9] & 0b1111)
            second = (npimg[10] >> 4) * 10 \
                     + (npimg[10] & 0b1111)
            microsecond = (npimg[11] >> 4) * 10 ** 5 \
                          + (npimg[11] & 0b1111) * 10 ** 4 \
                          + (npimg[12] >> 4) * 10 ** 3 \
                          + (npimg[12] & 0b1111) * 10 ** 2 \
                          + (npimg[13] >> 4) * 10 \
                          + (npimg[13] & 0b1111)
            timestamp = datetime.datetime(
                year, month, day, hour, minute, second, microsecond, tzinfo=None)
        else:
            imgCounter = None
            timestamp = None
        npimg = npimg.reshape((curBuf.yRes.value, curBuf.xRes.value))
        self.logger.debug(
            "buffer2numpy successful with buffer {}".format(curBuf.number.value))
        return npimg, timestamp, imgCounter
