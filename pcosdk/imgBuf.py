import ctypes
from ctypes.wintypes import DWORD, HANDLE, WORD
from win32event import WaitForSingleObject

# Small shortcut
cInt = ctypes.c_int


class ImgBuf():
    '''buffer class for buffer management in pco.sdk.

    This class is used for the buffer management in class control. Each
    instance of class buffer can be used to get an image. No functions
    are implemented.

    :param number: number of buffer. Default -1 for allocation via SDK
    :type number: ctypes.c_short, int
    :param size: buffersize in bits. Default 0
    :type size: ctypes.c_int
    :param bitpix: bits per pixel. Default 12
    :type bitpix: ctypes.c_int
    :param number: number of buffer. Default -1
    :type number: ctypes.c_int
    :param bitpix: bits per pixel. Default 12
    :type bitpix: ctypes.c_int
    :param xRes: pixels in x-direction of image. Default 0
    :type xRes: ctypes.wintypes.WORD
    :param yRes: pixels in y-direction of image. Default 0
    :type yRes: ctypes.wintypes.WORD
    :param buffer: buffer provided by python.
    :type buffer: WORD array
    :param address: address of buffer.
    :type address: ctypes.POINTER(WORD)
    :param hEvent: Event handler. Default None
    :type hEvent: ctypes.wintypes.HANDLE
    :param doubleImage: identification of double image mode. Default False
    :type boolean: boolean
    :param wBinHorz: Horizontal binning
    :type wBinHorz: WORD
    :param wBinVert: Vertical binning
    :type wBinVert: WORD
    :param wBitAlignment: Bit Alignment to MSB or LSB
    :type wBitAlignment: WORD
    
    Example::

        # Initiate an instance of buffer
        bufInst = imgBuf()
        # Create an array of buffers
        bufArray = []
        for i in range(10):
        bufArray.append(imgBuf())

    '''
    def __init__(self, bufNr=ctypes.c_short(-1), bitpix=cInt(-1)):
        '''Constructor
        '''
        if type(bufNr) == int:
            self.number = ctypes.c_short(bufNr)
        else:
            self.number = bufNr
        self.size = None
        if type(bitpix) == int:
            self.bitpix = cInt(bitpix)
        else:
            self.bitpix = bitpix
        self.xRes = WORD(-1)
        self.yRes = WORD(-1)
        self.buffer = None
        self.address = None
        self.hEvent = HANDLE(None)
        self.doubleImage = False
        self.wBinHorz = WORD(0)
        self.wBinVert = WORD(0)
        self.wBitAlignment = WORD(255)

    def getStatus(self):
        ''' Get current buffer status with WaitForSingleObject

        :returns: status
        :rtype: int
        '''
        status = WaitForSingleObject(self.hEvent, 0)
        return status
