# pyPcoSdk
Python wrapper for the pco.sdk with a pythonic image buffer class.

## Requirements
* pco.sdk
* Windows 64bit
* MS Visual C++ runtime
* Numpy

Tested with
* Win 10
* Anaconda 2018.12
* Python 3.6.8
* Visual Build Tools 2017 installed (VS2015 packages included)

## Installation
From a command prompt with your Python environment, run

```python
python setup.py install
```
The error messaging system is realized as C Extension and will be compiled during installation.


## Classes
### pcoCam
The class `pcoCam` has two main purposes.

First, it is a wrapper for the C functions from pco.sdk. After initialization, the commands can be accessed from the
object, e.g.
```
cam = PcoCam()
cam.PCO_ClearRamSegment()
```
Second, the class object handles a copy of the camera parameters, which can be accessed directly
```
print(cam.wXResAct)
```

**Please note**
* _A direct change of the value has no influence on the current parameter set in the camera!_
 Use the appropriate function from the SDK
* The values of the class attributes are `ctypes`

#### Logging
The class `pcoCam` supports logging with an initialized logger during acquisition (see example).

### ImgBuf
The class `ImgBuf` is a class to realize the buffer management in a pythonic way. No casting or
management of the buffer have to be done by the user.
An initialized buffer object can be handed over to the camera object, e.g.
```
curBuf = imgBuf.ImgBuf()
cam.PCO_AllocateBuffer(curBuf)
# Here comes the acquisition part...
image, tstamp, imgCount = cam.buffer2Numpy(curBuf)
```
The additional function `buffer2Numpy` converts the buffer the an numpy array and respective metadata.
## Example
An example is provided in `example.py`.
