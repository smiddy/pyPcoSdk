import ctypes
import logging
from pcosdk import control, imgBuf
import numpy as np
import matplotlib.pyplot as plt
from time import sleep

# Setup some type definitions
cInt = ctypes.c_int
int32 = ctypes.c_long
uInt32 = ctypes.c_ulong
uInt64 = ctypes.c_ulonglong
float64 = ctypes.c_double

if __name__ == '__main__':
    # Set number of images
    noImgs = 3
    # Create logger
    logging.basicConfig(level=logging.DEBUG, filename='example.log',
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # Open camera with context manager
    with control.PcoCam() as cam:
        cam.PCO_ClearRamSegment()
        cam.PCO_SetActiveRamSegment(1)
        cam.PCO_SetBitAlignment(0)
        cam.PCO_SetTriggerMode(1)
        cam.PCO_SetAcquireMode(cam.wAcquMode.value)
        cam.PCO_SetTimestampMode(1)
        cam.PCO_SetNoiseFilterMode(1)
        sCCDTemp, sCamTemp, sPowTemp = cam.PCO_GetTemperature()
        cam.PCO_SetDoubleImageMode(0)
        #         cam.PCO_SetBinning(2,1)
        #         cam.PCO_SetROI(99, 200, 500, 400)
        cam.PCO_ArmCamera()
        curBuf = imgBuf.ImgBuf()
        cam.PCO_GetSizes()
        # Create ouput variables
        images = np.empty((noImgs, cam.wYResAct.value, cam.wXResAct.value), dtype='uint16')
        tstamps = [None] * noImgs
        imgCount = [None] * noImgs
        cam.PCO_AllocateBuffer(curBuf)
        bla1, bla2 = cam.PCO_GetBufferStatus(curBuf)
        cam.PCO_SetImageParameters(curBuf)
        cam.PCO_SetRecordingState(1)
        print("Busy: ", cam.PCO_GetCameraBusyStatus())
        for ii in range(noImgs):
            cam.PCO_ForceTrigger()
            cam.PCO_GetImageEx(curBuf, 1, 0)
            images[ii, :, :], tstamps[ii], imgCount[ii] = cam.buffer2Numpy(curBuf)
            print('(no imgs, total):', cam.PCO_GetNumberOfImagesInSegment(1))
        cam.PCO_SetRecordingState(0)
        print('no imgs:', cam.PCO_GetNumberOfImagesInSegment(1))
    np.save('testi.npy', images)
    # plt.imshow(testi)
    # fig = plt.figure()
    # a=fig.add_subplot(1,2,1)
    # plt.imshow(testi[0:1200,:])
    # a.set_title('1st acquisition')
    # a=fig.add_subplot(1,2,2)
    # plt.imshow(testi[1200:2400,:])
    # a.set_title('2nd acquisition')
    # plt.show()
    # plt.imsave('example.tiff', testi[0:1200, :])
    print("This works!")
