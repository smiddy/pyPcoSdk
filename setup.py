from setuptools import setup, Extension
from distutils.util import convert_path

# Set the current version
main_ns = {}
ver_path = convert_path('pcosdk/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)
__version__ = main_ns['__version__']


pcoErrorModule = Extension("pcoError",
                           sources=["pcoError.c"],
                           include_dirs=['C:\Program Files (x86)'
                                         '\Digital Camera Toolbox'
                                         '\pco.sdk\include'],
                           define_macros=[("PCO_ERRT_H_CREATE_OBJECT", None)],
                           )
setup(
      name="pyPcoSdk",
      description="A wrapper for the pco.sdk",
      version=__version__,
      platforms=["win-amd64"],
      author="Markus J. Schmidt",
      author_email='schmidt@ifd.mavt.ethz.ch',
      license="GNU GPLv3",
      url="https://gitlab.ethz.ch/ifd-lab/device-driver/pyPcoSdk",
      packages=["pcosdk"],
      ext_modules=[pcoErrorModule],
      install_requires=['numpy>=1.9.2'],
      zip_safe=False,
      classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Console',
                   'Intended Audience :: Science/Research',
                   'Intended Audience :: Education',
                   'Topic :: Scientific/Engineering',
                   'Operating System :: Microsoft :: Windows',
                   'License :: OSI Approved :: GNU General Public License v3 '
                   'or later (GPLv3+)',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',
                   'Topic :: Scientific/Engineering :: Visualization'
                   ]
      )
